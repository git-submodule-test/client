#include <iostream>
#include <string>

#include <spdlog/spdlog.h>
#include <zmq.hpp>

#include "message.hpp"

namespace logging = spdlog;

int main()
{
    zmq::context_t context;
    zmq::socket_t socket{context, zmq::socket_type::req};
    logging::info("Connecting to hello world server…");
    socket.connect("tcp://localhost:5555");

    for (int i = 0; i != 10; i++)
    {
        Message request;
        request.who("Server");
        logging::info("Sending: {}", request);
        socket.send(request.pack(), zmq::send_flags::none);

        zmq::message_t rep;
        socket.recv(rep, zmq::recv_flags::none);
        Message reply{rep};
        logging::info("Received: {}", reply);
    }
    return 0;
}
